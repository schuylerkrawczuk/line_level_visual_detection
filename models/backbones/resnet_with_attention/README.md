# ResNet with Visual Attention

Implementation of self-attention used in a spatial context with images. 
Implementated based off of "Non-local neural networks" [^1].
## Motivation
The attention mechanism creates the feature for a position as a function
of all other positions by taking a weighted average of them in an embedding space. 
In the context of image detection, this creates feature maps that are non-local, as opposed
convolutions which only work over a local spatial region. 

The motivation for using this with column/cell detection is that contrary to tables, where the 
out-of-the-box R-CNN performs well, columns and cells are frequently do not contain strong local 
features within their region proposals, and require more context from the neighboring region to 
identify them. This leads to an ill-posed classification problem based on local features, resulting
in poor objectness scores for correct region proposals, and poor model performance. The hope is for
the creation of feature maps based on non-local spatial regions to provide better context and improve 
objectness of the target regions


## Integration with ResNet
As outlined in the above paper, a non-local block is inserted into the ResNet architecture as a residual
block between the third and fourth residual blocks. The weight and bias of the non-local block 
are simply initialized as zero to allow for leveraging of pre-trained weights still, without breaking the
functionality. 

[^1]: https://arxiv.org/pdf/1711.07971.pdf