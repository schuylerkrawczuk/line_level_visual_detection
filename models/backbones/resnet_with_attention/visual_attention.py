from torch import nn
import torch
import numpy as np


class NonLocalBlock(nn.Module):
    """
    Implementation of non-local block from
    "Non-local Neural Networks" <https://arxiv.org/pdf/1711.07971.pdf>"
    """
    def __init__(self, n_channels):
        super().__init__()
        self.phi = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.theta = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.g = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.w = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        batch_size, n_channels, width, height = x.size()
        hw = width * height

        phi_theta = torch.bmm(
            self.theta(x).view(batch_size, n_channels, hw).permute(0, 2, 1),
            self.phi(x).view(batch_size, n_channels, hw))
        phi_theta = self.softmax(phi_theta)

        phi_theta_g = torch.bmm(
            phi_theta,
            self.g(x).view(batch_size, n_channels, hw).permute(0, 2, 1))
        phi_theta_g = phi_theta_g.view(batch_size, n_channels, width, height)

        z = self.w(phi_theta_g) + x
        return z


class SpatialAttentionBlock(nn.Module):
    """
    Implementation of relative spatial self-attention block from
    "Stand-Alone Self-Attention in Vision Models" <https://arxiv.org/pdf/1906.05909.pdf>"
    """

    def __init__(self, n_channels):
        super().__init__()
        self.phi = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.theta = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.g = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.w = nn.Conv2d(n_channels, n_channels, 1, 1)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        batch_size, n_channels, width, height = x.size()
        hw = width * height

        inds = np.indices((width, height))
        r = torch.tensor([inds - i[:, None, None] for i in inds.reshape(2, -1).T])
        r = r.permute(1, 0, 2, 3).view(2, hw, hw)
        # r: (batch_size, hw, width, height) -> (batch_size, hw, hw)

        phi = self.phi(x).view(batch_size, n_channels, hw)
        theta = self.theta(x).view(batch_size, n_channels, hw).permute(0, 2, 1)
        phi_theta = torch.bmm(phi, theta)

        # spatial_embedding = torch.bmm(phi, ____)
        # phi_theta = self.softmax(phi_theta + spatial_embedding)
        # TODO: finish this ^

        phi_theta_g = torch.bmm(
            phi_theta,
            self.g(x).view(batch_size, n_channels, hw).permute(0, 2, 1))
        phi_theta_g = phi_theta_g.view(batch_size, n_channels, width, height)

        z = self.w(phi_theta_g) + x
        return z
