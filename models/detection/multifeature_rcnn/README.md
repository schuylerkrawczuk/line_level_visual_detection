# Visual-Text Dual Feature Classification

This is an inital attempt at implementing a object detector that uses text features
with region proposals for classifcation along with the visual features. In the context of 
table detection, this will be useful for unifying the text/visual models and better performance
of the visual model on tables with weak visual features, i.e. unstructured, single line tables

To do: 
- Need a significant amount of data to train/evaluate on. And/or, come up with an initialization
scheme for the text features so that the pre-trained weights can be used with it. I
am created data from OCR dataframes of invoices 
- Need a proper choice of word embedding. Currently using pre-trained that probably won't
generalize well to this domain