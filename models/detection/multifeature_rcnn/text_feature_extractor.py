from typing import List
from torchvision.ops.boxes import _box_inter_union
from torchtext.vocab import GloVe
from torch import nn, Tensor
import torch


class DummyVectorizer(nn.Module):
    def __init__(self):
        super().__init__()
        self.embedding_length = 300
        self.vectors = torch.ones((1000, 300))


class TextFeatureExtractor(nn.Module):
    def __init__(self, vectorizer=None):
        super().__init__()
        self.vectorizer = vectorizer
        if self.vectorizer is None:
            self.vectorizer = GloVe()
        self.vectors = self.vectorizer.vectors
        self.embedding_length = 300
        self.intersect_threshold = 0.8

    def forward(self, proposals: List[Tensor], ocr: List[Tensor]) -> List[Tensor]:
        """
        gets text from region proposal boxes and creates embedding

        Args:
            proposals (List[Tensor]): proposal bounding boxes
            ocr (List[Tensor]): text token and position (x0, y0, x1, y1, token)
        """
        text_features = []
        for proposal, text in zip(proposals, ocr):  # TODO: nomenclature - proposal is bad
            # get token embedding for all tokens
            embeddings = torch.index_select(self.vectors, 0, text[:, 4].cpu().int())

            # get text inside each proposal
            inter, _ = _box_inter_union(proposal, text[:, :4])
            text_in_proposal = inter > self.intersect_threshold

            # TODO: all this shiz is funky around the empties
            # get mean embedding inside each proposal
            vecs_per_proposal = [embeddings[text_in_proposal[i]] for i in range(text_in_proposal.shape[0])]
            try:
                mean_proposal_embeddings = torch.vstack(
                    [proposal_vecs.mean(dim=0) if len(proposal_vecs) else torch.zeros(self.embedding_length)
                     for proposal_vecs in vecs_per_proposal]
                )
            except RuntimeError:
                mean_proposal_embeddings = torch.zeros((len(vecs_per_proposal), self.embedding_length))

            text_features.append(mean_proposal_embeddings.to(proposals[0].device))

        return text_features
