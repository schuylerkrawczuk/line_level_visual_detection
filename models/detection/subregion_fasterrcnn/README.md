# Sub-region R-CNN
This was designed for the purpose of cell detection within tables. In training, the R-CNN 
samples balanced foreground-background region proposals from across the entire image to 
calculate loss. In the context of cell detection, we only really care about the performance
inside of the tables, and proposals outside of this area may skew learning. In this implementation
of the region proposal network, proposals outside of specified subregions (tables in this case) are 
not considered in calculating training loss.