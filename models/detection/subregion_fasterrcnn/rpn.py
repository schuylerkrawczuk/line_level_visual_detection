from typing import List, Tuple, Dict, Optional
from torch import Tensor
from torchvision.models.detection.rpn import RegionProposalNetwork, concat_box_prediction_layers
from torchvision.models.detection.image_list import ImageList
from torchvision.ops import boxes as box_ops
import torch


class SubregionProposalNetwork(RegionProposalNetwork):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def forward(
            self,
            images: ImageList,
            features: Dict[str, Tensor],
            targets: Optional[List[Dict[str, Tensor]]] = None,
            regions_of_interest: Optional[List[Tensor]] = None
    ) -> Tuple[List[Tensor], Dict[str, Tensor]]:

        """
        Args:
            images (ImageList): images for which we want to compute the predictions
            features (Dict[str, Tensor]): features computed from the images that are
                used for computing the predictions. Each tensor in the list
                correspond to different feature levels
            targets (List[Dict[str, Tensor]]): ground-truth boxes present in the image (optional).
                If provided, each element in the dict should contain a field `boxes`,
                with the locations of the ground-truth boxes.
            regions_of_interest (List[Tensor]): bounding boxes of regions of interest in image to sample proposals from
        Returns:
            boxes (List[Tensor]): the predicted boxes from the RPN, one Tensor per
                image.
            losses (Dict[str, Tensor]): the losses for the model during training. During
                testing, it is an empty dict.
        """
        # RPN uses all feature maps that are available
        features = list(features.values())
        objectness, pred_bbox_deltas = self.head(features)
        anchors = self.anchor_generator(images, features)

        num_images = len(anchors)
        num_anchors_per_level_shape_tensors = [o[0].shape for o in objectness]
        num_anchors_per_level = [s[0] * s[1] * s[2] for s in num_anchors_per_level_shape_tensors]
        objectness, pred_bbox_deltas = concat_box_prediction_layers(objectness, pred_bbox_deltas)

        if self.training:
            # keep track of anchors outside of region of interest
            training_keep_inds = self.filter_subregion_anchors(anchors, regions_of_interest)

        # apply pred_bbox_deltas to anchors to obtain the decoded proposals
        # note that we detach the deltas because Faster R-CNN do not backprop through
        # the proposals
        proposals = self.box_coder.decode(pred_bbox_deltas.detach(), anchors)
        proposals = proposals.view(num_images, -1, 4)
        boxes, scores = self.filter_proposals(proposals, objectness, images.image_sizes, num_anchors_per_level)

        losses = {}
        if self.training:
            assert targets is not None

            # remove samples outside of subregions for evaluating loss
            training_keep_inds_flat = torch.cat(training_keep_inds)
            subregion_anchors = [anchors[idx][keep_inds] for idx, keep_inds in enumerate(training_keep_inds)]
            subregion_pred_bbox_deltas = pred_bbox_deltas[training_keep_inds_flat]
            subregion_objectness = objectness[training_keep_inds_flat]

            labels, matched_gt_boxes = self.assign_targets_to_anchors(subregion_anchors, targets)
            regression_targets = self.box_coder.encode(matched_gt_boxes, subregion_anchors)
            loss_objectness, loss_rpn_box_reg = self.compute_loss(
                subregion_objectness, subregion_pred_bbox_deltas, labels, regression_targets
            )
            losses = {
                "loss_objectness": loss_objectness,
                "loss_rpn_box_reg": loss_rpn_box_reg,
            }
        return boxes, losses

    @staticmethod
    def filter_subregion_anchors(
            anchors: List[Tensor],
            subregions: List[Tensor],
    ) -> List[Tensor]:
        """
        Filter out anchors outside of subregions
        :return:
        """
        keep_inds = []
        for batch_id, (anchor_batch, subregion_batch) in enumerate(zip(anchors, subregions)):
            inter, _ = box_ops._box_inter_union(anchor_batch, subregion_batch)
            keep_inds.append(torch.any(inter > 0.2, dim=1))

        return keep_inds
