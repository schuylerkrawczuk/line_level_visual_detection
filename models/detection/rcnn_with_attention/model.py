from typing import Optional, List
from torchvision.ops import misc as misc_nn_ops
from torchvision.ops.feature_pyramid_network import LastLevelMaxPool, ExtraFPNBlock
from models.backbones.resnet_with_attention.model import resnet50_with_attention
from torchvision.models.detection.backbone_utils import _validate_trainable_layers, BackboneWithFPN
from torchvision.models.detection.faster_rcnn import FasterRCNN, model_urls
from torchvision.models import resnet
from models.utils import resnet_attention_load_state_dict, overwrite_eps
import torch


def fasterrcnn_resnet50_attention_fpn(
    pretrained=False, progress=True, num_classes=91, pretrained_backbone=True, trainable_backbone_layers=None, **kwargs
):
    trainable_backbone_layers = _validate_trainable_layers(
        pretrained or pretrained_backbone, trainable_backbone_layers, 6, 4
    )

    if pretrained:
        # no need to download the backbone if pretrained is set
        pretrained_backbone = False

    backbone = resnet50_with_attention(pretrained=pretrained_backbone, progress=progress, norm_layer=misc_nn_ops.FrozenBatchNorm2d)
    backbone = _resnet_with_attention_fpn_extractor(backbone, trainable_backbone_layers)
    model = AttentonFasterRCNN(backbone, num_classes, 'backbone.body.nl_block', 'backbone.body.nl_block.phi.weight', **kwargs)
    if pretrained:
        state_dict = torch.hub.load_state_dict_from_url(model_urls["fasterrcnn_resnet50_fpn_coco"], progress=progress)
        model.load_state_dict(state_dict)
        overwrite_eps(model, 0.0)
    return model


class AttentonFasterRCNN(FasterRCNN):
    def __init__(self, backbone, num_classes, custom_key_prefix, custom_check_key, **kwargs):
        super().__init__(backbone, num_classes, **kwargs)
        self.custom_key_prefix = custom_key_prefix
        self.custom_check_key = custom_check_key

    def load_state_dict(self, state_dict, strict=True):
        state_dict = resnet_attention_load_state_dict(self, state_dict, backbone=True)
        super().load_state_dict(state_dict, strict=strict)


def _resnet_with_attention_fpn_extractor(
    backbone: resnet.ResNet,
    trainable_layers: int,
    returned_layers: Optional[List[int]] = None,
    extra_blocks: Optional[ExtraFPNBlock] = None,
) -> BackboneWithFPN:

    # select layers that wont be frozen
    assert 0 <= trainable_layers <= 6
    layers_to_train = ["layer4", 'nl_block', "layer3", "layer2", "layer1", "conv1"][:trainable_layers]
    if trainable_layers == 6:
        layers_to_train.append("bn1")
    for name, parameter in backbone.named_parameters():
        if all([not name.startswith(layer) for layer in layers_to_train]):
            parameter.requires_grad_(False)

    if extra_blocks is None:
        extra_blocks = LastLevelMaxPool()

    if returned_layers is None:
        returned_layers = [1, 2, 3, 4]
    assert min(returned_layers) > 0 and max(returned_layers) < 5
    return_layers = {f"layer{k}": str(v) for v, k in enumerate(returned_layers)}

    in_channels_stage2 = backbone.inplanes // 8
    in_channels_list = [in_channels_stage2 * 2 ** (i - 1) for i in returned_layers]
    out_channels = 256
    return BackboneWithFPN(backbone, return_layers, in_channels_list, out_channels, extra_blocks=extra_blocks)

