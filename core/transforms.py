import numpy as np
import torch
import torchvision
import torchvision.transforms.functional as F

# TODO: transform for image only

def get_transforms(eval=False, resize=True):
    """ Returns composition of transforms to apply to image before input to model

        Args:
        eval (bool): images being passed are for evaluation (vs. train). if True, random transforms removed
    """
    transform_list = [
        image_to_tensor,
        resize_image,
        random_horizontal_flip,
        normalize_image,
        pad_to_multiple
    ]
    if eval:
        transform_list = [
            image_to_tensor,
            resize_image,
            normalize_image,
            pad_to_multiple
        ]

    if not resize:
        transform_list.pop(1)

    return torchvision.transforms.Compose(transform_list)


def image_to_tensor(sample):
    """
    convert image to torch tensor
    """
    image = sample['image']
    image = F.to_tensor(image)
    sample['image'] = image
    return sample


def resize_image(sample, x_new=800, y_new=1333):
    """
    Resize image and associated bounding boxes
    """
    image = sample['image']
    boxes = sample['boxes']
    area = sample['area']

    y_shape = image.shape[1]
    x_shape = image.shape[2]

    # resize image
    image = F.resize(image, (y_new, x_new))
    sample['image'] = image

    # update area and bboxes
    if len(boxes):
        area = area / (y_shape * x_shape) * x_new * y_new

        boxes[:, ::2] *= x_new / x_shape
        boxes[:, 1::2] *= y_new / y_shape
        sample['boxes'] = boxes
        sample['area'] = area

    return sample


def random_horizontal_flip(sample, prob=0.5):
    """flip image horizontally with a probability of prob

        Args:
        prob (float): probability of image being flipped
    """
    if torch.rand(1) < prob:
        image = sample['image']
        boxes = sample['boxes']

        image = F.hflip(image)
        sample['image'] = image

        if len(boxes):
            boxes[:, ::2] = torch.flip(image.shape[2] - boxes[:, ::2], [1])
            sample['boxes'] = boxes

    return sample


def normalize_image(sample):
    """
    Normalize image per ImageNet norm stats
    """
    img_norm_cfg = dict(
        mean=[123.675, 116.28, 103.53],
        std=[58.395, 57.12, 57.375])

    sample['image'] = F.normalize(255 * sample['image'], img_norm_cfg['mean'], img_norm_cfg['std'])
    return sample


def pad_to_multiple(sample, divisor=32, pad_val=0):
    """ Pad the image so each side is a multiple of divisor.
        Pads right and bottom, does not affect bbox annotation

        Args:
        divisor (int): each side must be a multiple of this value
        pad_val (int): pixel value of the padding
    """
    image = sample['image']
    pad_x = int(np.ceil(image.shape[2] / divisor)) * divisor - image.shape[2]
    pad_y = int(np.ceil(image.shape[1] / divisor)) * divisor - image.shape[1]
    image = F.pad(image, (0, 0, pad_x, pad_y), fill=pad_val)

    sample['image'] = image

    return sample


def torch_to_numpy(image):
    """
    convert a torch image to cv2 image format and normalize to [0,1]. (n x C x H x W) -> (H x W x C)
    """
    image = torch.squeeze(image).permute(1, 2, 0).numpy()
    image = 255 * (image - image.min()) / (image.max() - image.min())
    return image
