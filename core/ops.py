from torchvision.ops.boxes import _box_inter_union, _upcast
from torch import Tensor
import torch


def giou_loss(box_preds: Tensor, box_targets: Tensor) -> Tensor:
    inter, union = _box_inter_union(box_preds, box_targets)

    # only comparing corresponding indices of each tensor
    inter = torch.diag(inter)
    union = torch.diag(union)

    iou = inter / union

    lti = torch.min(box_preds[:, :2], box_targets[:, :2])
    rbi = torch.max(box_preds[:, 2:], box_targets[:, 2:])

    whi = _upcast(rbi - lti)
    areai = whi[:, 0] * whi[:, 1]

    giou = iou - (areai - union) / areai
    return (1 - giou).sum()
