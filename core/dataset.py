import os
import torch
import pandas as pd
from PIL import Image, ImageFile
from pycocotools.coco import COCO
from torchtext.vocab import GloVe

ImageFile.LOAD_TRUNCATED_IMAGES = True


class TableDataSet(torch.utils.data.Dataset):

    def __init__(self, img_dir, annotation_path, transforms=None):
        self.root = img_dir
        self.transforms = transforms
        self.coco = COCO(annotation_path)
        self.ids = list(sorted(self.coco.imgs.keys()))

    def __getitem__(self, index):
        return self._get_item(index)

    def _get_item(self, index):
        img_id = self.ids[index]
        ann_ids = self.coco.getAnnIds(imgIds=img_id)

        # annotation
        coco_annotation = self.coco.loadAnns(ann_ids)

        # images
        path = self.coco.loadImgs(img_id)[0]['file_name']
        img = Image.open(os.path.join(self.root, path)).convert('RGB')

        # num objects in image
        num_objs = len(coco_annotation)

        boxes, labels, areas = [], [], []
        # bboxes:
        # coco format = [x0, y0, w, h]
        # pytorch format = [x0, y0, x1, y1]
        for i in range(num_objs):  # TODO: function this, reuse across classes
            xmin = coco_annotation[i]['bbox'][0]
            ymin = coco_annotation[i]['bbox'][1]
            xmax = xmin + coco_annotation[i]['bbox'][2]
            ymax = ymin + coco_annotation[i]['bbox'][3]

            # double-check correct order for coordinates
            x_coords = sorted([xmin, xmax])
            y_coords = sorted([ymin, ymax])
            boxes.append([x_coords[0], y_coords[0], x_coords[1], y_coords[1]])

            areas.append(coco_annotation[i]['area'])
            labels.append(coco_annotation[i]['category_id'])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)

        if len(boxes) == 0:
            boxes = torch.empty((0, 4), dtype=torch.float32)

        labels = torch.as_tensor(labels, dtype=torch.int64)
        areas = torch.as_tensor(areas, dtype=torch.float32)

        img_id = torch.tensor([img_id])

        annotations = dict(
            image=img,
            boxes=boxes,
            labels=labels,
            image_id=img_id,
            area=areas,
        )

        if self.transforms is not None:
            annotations = self.transforms(annotations)

        image = annotations.pop('image')

        return image, annotations

    def __len__(self):
        return len(self.ids)


class SubregionDataSet(TableDataSet):
    """
        Trains R-CNN only on objects within subregions of image. These are specified with annotations
        of id 'subregion_id'
    """

    def __init__(self, img_dir, annotation_path, subregion_id=1, transforms=None):
        self.root = img_dir
        self.transforms = transforms
        self.coco = COCO(annotation_path)
        self.ids = list(sorted(self.coco.imgs.keys()))
        self.subregion_id = subregion_id

    def separate_subregion_annotation(self, ann):

        subregions = ann['boxes'][ann['labels'] == self.subregion_id]
        keep_ann = ann['labels'] != self.subregion_id
        ann['boxes'] = ann['boxes'][keep_ann]
        ann['labels'] = ann['labels'][keep_ann]
        ann['area'] = ann['area'][keep_ann]
        ann['subregions'] = subregions
        return ann

    def __getitem__(self, index):
        image, ann = self._get_item(index)
        ann = self.separate_subregion_annotation(ann)
        return image, ann


class ImageTextDataSet(TableDataSet):
    """
        For training multi-feature rcnn. Gets text from OCR within the bounding box of target objects
    """
    def __init__(self, img_dir, ocr_dir, annotation_path, transforms=None):
        super().__init__(img_dir, annotation_path, transforms=transforms)
        self.ocr_dir = ocr_dir
        self.text_embedding = GloVe()

    def __getitem__(self, index):
        image, annotations = self._get_item(index)
        ocr = self._get_ocr(index, image)
        image = {'image': image, 'ocr': ocr}
        return image, annotations

    def _get_ocr(self, index, image):
        csv_file = self.coco.imgs[self.ids[index]]['file_name'].split('.')[0] + '.csv'
        ocr_df = pd.read_csv(os.path.join(self.ocr_dir, csv_file), index_col=0)
        ocr_df[['x0_norm', 'x1_norm']] *= image.shape[2]
        ocr_df[['y0_norm', 'y1_norm']] *= image.shape[1]

        ocr_df['token_index'] = ocr_df.text.apply(self.map_text_to_token_index)
        ocr_df = ocr_df[ocr_df.token_index > -1]
        ocr_df = ocr_df.drop('text', axis=1)
        ocr = torch.tensor(ocr_df.values)
        # TODO: do we need to transform these boxes?

        return ocr

    def map_text_to_token_index(self, token):
        try:
            index = self.text_embedding.stoi[token]
        except KeyError:
            index = -1
        return index


class GeneralTableDataSet(TableDataSet):
    """
        For general table annotations lacking anything other than bbox
    """
    def __init__(self, img_dir, annotation_path, transforms=None):
        super().__init__(img_dir, annotation_path, transforms=transforms)

    def __getitem__(self, index):
        img_id = self.ids[index]
        ann_ids = self.coco.getAnnIds(imgIds=img_id)

        # annotation
        coco_annotation = self.coco.loadAnns(ann_ids)

        # images
        path = self.coco.loadImgs(img_id)[0]['file_name']
        img = Image.open(os.path.join(self.root, path)).convert('RGB')

        # num objects in image
        num_objs = len(coco_annotation)

        # bboxes:
        # coco format = [x0, y0, w, h]
        # pytorch format = [x0, y0, x1, y1]
        boxes = []
        for i in range(num_objs):
            xmin = coco_annotation[i]['bbox'][0]
            ymin = coco_annotation[i]['bbox'][1]
            xmax = xmin + coco_annotation[i]['bbox'][2]
            ymax = ymin + coco_annotation[i]['bbox'][3]

            # double-check correct order for coordinates
            x_coords = sorted([xmin, xmax])
            y_coords = sorted([ymin, ymax])
            boxes.append([x_coords[0], y_coords[0], x_coords[1], y_coords[1]])
        boxes = torch.as_tensor(boxes, dtype=torch.float32)

        if len(boxes) == 0:
            boxes = torch.empty((0, 4), dtype=torch.float32)

        labels = torch.ones((num_objs,), dtype=torch.int64)
        img_id = torch.tensor([img_id])

        areas = []
        for i in range(num_objs):
            areas.append(coco_annotation[i]['area'])
        areas = torch.as_tensor(areas, dtype=torch.float32)

        annotations = dict(
            image=img,
            boxes=boxes,
            labels=labels,
            image_id=img_id,
            area=areas,
        )

        if self.transforms is not None:
            annotations = self.transforms(annotations)

        image = annotations.pop('image')

        return image, annotations

    def __len__(self):
        return len(self.ids)


class ImageOnlyLoader(torch.utils.data.Dataset):
    """
    For loading images in a directory w/o annotation
    """
    def __init__(self, img_dir, annotation_path, transforms=None):
        self.root = img_dir
        self.transforms = transforms
        self.file_names = os.listdir(img_dir)
        self.ids = range(len(self.file_names))

    def __getitem__(self, index):

        img = Image.open(os.path.join(self.root, self.file_names[index])).convert('RGB')

        annotations = dict(
            image=img,
            boxes=torch.empty((0, 4), dtype=torch.float32),
            labels=torch.empty((0, 1), dtype=torch.int64),
            image_id=torch.empty((0, 1), dtype=torch.int64),
            area=torch.empty((0, 1), dtype=torch.float32),
        )
        if self.transforms is not None:
            annotations = self.transforms(annotations)
        image = annotations.pop('image')

        return image, {'file_name': self.file_names[index]}

    def __len__(self):
        return len(self.ids)
