import os
import time
import torch
import pandas as pd
from torch import nn


class Trainer:
    def __init__(self, input_dict, params):
        self.model = input_dict['model']
        self.optimizer = input_dict['optimizer']
        self.scheduler = input_dict['lr_scheduler'] if 'lr_scheduler' in input_dict else None
        self.data_loaders = input_dict['data_loaders']
        self.params = params
        self.epoch = 0
        self.params['phases'] = [p for p in self.data_loaders]
        self.log = {p: pd.DataFrame() for p in self.params['phases']}
        self.init_model()

    def init_model(self):
        if not os.path.isdir(self.params['model_dir']):
            os.mkdir(self.params['model_dir'])

        if 'checkpoint_file' in self.params and self.params['checkpoint_file'] is not None:
            checkpoint = torch.load(os.path.join(self.params['model_dir'], self.params['checkpoint_file']),
                                    map_location=self.params['device'])
            model_state_dict = checkpoint['model_state_dict']
            opt_state_dict = checkpoint['optimizer_state_dict']
            self.epoch = checkpoint['epoch'] + 1
            self.model.load_state_dict(model_state_dict)
            self.model.to(self.params['device'])

            # Loading old optimizer overrides choice of optimization parameters in current config
            # leaving this out for now
                #self.optimizer.load_state_dict(opt_state_dict)
            # except ValueError:
            #    print('optimizer state cannot be loaded from checkpoint')

            if self.scheduler is not None:
                if 'scheduler_state_dict' in checkpoint:
                    self.scheduler.load_state_dict(checkpoint['scheduler_state_dict'])

            print(f'resuming from {self.params["checkpoint_file"]} at epoch {self.epoch}')

    def train(self, n_epochs):
        self.model.to(self.params['device'])
        self.model.train()

        for epoch in range(self.epoch, n_epochs):
            for phase in self.params['phases']:
                print(32 * '-' + f' {phase} ' + 32 * '-')
                phase_loss = []
                for i, (imgs, annotations) in enumerate(self.data_loaders[phase]):
                    t_start = time.time()
                    if isinstance(imgs[0], torch.Tensor):
                        imgs = [img.to(self.params['device']) for img in imgs]
                    elif isinstance(imgs[0], dict):
                        imgs = [{k: v.to(self.params['device']) for k, v in im.items()} for im in imgs]

                    annotations = [{k: v.to(self.params['device']) for k, v in t.items()} for t in annotations]

                    loss_dict = self.model(imgs, annotations)
                    loss = sum(loss for loss in loss_dict.values())
                    phase_loss.append({k: loss_dict[k].cpu().detach().numpy() for k in loss_dict})

                    # backprop, optimization step
                    if phase == 'train':
                        self.optimizer.zero_grad()
                        loss.backward()

                        # gradient clipping
                        nn.utils.clip_grad_norm_(self.model.parameters(), max_norm=35.0, norm_type=2)
                        self.optimizer.step()

                    t_end = time.time()
                    print(f'Epoch {epoch: {5}}|  Iteration: {i: {5}}/{len(self.data_loaders[phase]): {5}}|  Loss: {loss: 5.5f}|  Time: {t_end - t_start: 5.5f}|')

                self.log[phase] = self.log[phase].append(pd.DataFrame(phase_loss).mean(), ignore_index=True)

            if self.scheduler is not None:
                self.scheduler.step()

            self.save_checkpoint(epoch, f'epoch_{epoch+1}.pth')

    def save_checkpoint(self, epoch, filename):
        checkpoint_path = os.path.join(self.params['model_dir'], filename)
        save_dict = {
            'epoch': epoch,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
        }

        if self.scheduler is not None:
            save_dict['scheduler_state_dict'] = self.scheduler.state_dict()

        torch.save(save_dict, checkpoint_path)

        for phase in self.params['phases']:
            self.log[phase].to_csv(os.path.join(self.params['model_dir'], f'{phase}_log.csv'))
