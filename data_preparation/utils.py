import os
import numpy as np
import pandas as pd
import json


def train_split(annotation_path, new_ann_dir, split: float):
    """
    split COCO dataset randomly into train/test set. split is percent of images put in train set
    """
    filename = annotation_path.split('/')[-1].split('.')[0]

    annotation = json.load(open(annotation_path))
    inds = np.arange(len(annotation['images']))
    np.random.shuffle(inds)
    split_ind = int(split * len(inds))
    train_inds = inds[:split_ind]
    test_inds = inds[split_ind:]

    train_images = np.array(annotation['images'])[train_inds].tolist()
    train_image_ids = np.unique([i['id'] for i in train_images])

    test_images = np.array(annotation['images'])[test_inds].tolist()
    test_image_ids = np.unique([i['id'] for i in test_images])

    image_ids = [i['image_id'] for i in annotation['annotations']]

    train_annotations = np.array(annotation['annotations'])[
        [i in train_image_ids for i in image_ids]].tolist()
    test_annotations = np.array(annotation['annotations'])[
        [i in test_image_ids for i in image_ids]].tolist()

    train_annotation_json = annotation.copy()
    train_annotation_json['annotations'] = train_annotations
    train_annotation_json['images'] = train_images

    test_annotation_json = annotation.copy()
    test_annotation_json['annotations'] = test_annotations
    test_annotation_json['images'] = test_images

    json.dump(train_annotation_json, open(os.path.join(new_ann_dir, f'{filename}_train.json'), 'w'))
    json.dump(test_annotation_json, open(os.path.join(new_ann_dir, f'{filename}_test.json'), 'w'))


def remove_bad_images(annotation_path, bad_file_list):
    """
    Remove images and corresponding annotations from JSON annotation file given a list of image file names
    """
    annotations = json.load(open(annotation_path, 'r'))

    good_image_ids = []
    good_images = []
    for im in annotations['images']:
        if im['file_name'] not in bad_file_list:
            good_image_ids.append(im['id'])
            good_images.append(im)

    good_ann = []
    for ann in annotations['annotations']:
        if ann['image_id'] in good_image_ids:
            good_ann.append(ann)

    annotations['images'] = good_images
    annotations['annotations'] = good_ann

    return annotations


def append_datasets(annotation_path_1, annotation_path_2):
    """
    Combine two COCO annotation sets into one json file.
    """
    #TODO: append sets w/ diff labels, same imgs
    if isinstance(annotation_path_1, str):
        annotation_1 = json.load(open(annotation_path_1, 'r'))
    elif isinstance(annotation_path_1, dict):
        annotation_1 = annotation_path_1

    annotation_2 = json.load(open(annotation_path_2, 'r'))

    assert pd.DataFrame(annotation_1['categories'])[['id', 'name']].compare(
           pd.DataFrame(annotation_2['categories'])[['id', 'name']]).empty

    ann2_image_id_start = pd.DataFrame(annotation_1['images'])['id'].max() + 1
    ann2_ann_id_start = pd.DataFrame(annotation_1['annotations'])['id'].max() + 1

    for im in annotation_2['images']:
        im['id'] = int(im['id'] + ann2_image_id_start)

    for ann in annotation_2['annotations']:
        ann['image_id'] = int(ann['image_id'] + ann2_image_id_start)
        ann['id'] = int(ann['id'] + ann2_ann_id_start)

    annotation_1['images'].extend(annotation_2['images'])
    annotation_1['annotations'].extend(annotation_2['annotations'])

    return annotation_1

