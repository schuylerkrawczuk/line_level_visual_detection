"""
visualization for coco annotations. Saves images with bboxes drawn on
"""

from core.dataset import TableDataSet
from core.transforms import get_transforms, torch_to_numpy
from typing import Dict
import os
import torch
import numpy as np
import cv2
import argparse
from tqdm import tqdm


def draw_bboxes(image: np.ndarray, bboxes: np.ndarray, labels: np.ndarray, colors: Dict, scores=None):
    image = cv2.UMat(image)

    if scores is not None:
        for box, label, score in zip(bboxes, labels, scores):
            image = cv2.rectangle(image, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), colors[label], 3)
            cv2.putText(image, f'{score: .2}', (int(box[0]), int(box[3]) + 14), cv2.FONT_HERSHEY_SIMPLEX, 0.6, colors[label], 2)
    else:
        for box, label in zip(bboxes, labels):
            image = cv2.rectangle(image, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), colors[label], 3)
    return image.get()


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='visualize annotations')
    argc.add_argument('img_dir', type=str, help='image directory')
    argc.add_argument('annotation_path', type=str, help='path for .json file for images')
    argc.add_argument('out_dir', type=str, help='path to save annotated images')
    argp = argc.parse_args()

    if not os.path.isdir(argp.out_dir):
        os.mkdir(argp.out_dir)

    dataset = TableDataSet(argp.img_dir, argp.annotation_path, get_transforms(eval=True))
    loader = torch.utils.data.DataLoader(dataset,
                                         shuffle=False,
                                         num_workers=0,
                                         batch_size=1
                                         )

    colors = {1: (255, 0, 0), 2: (0, 255, 0), 3: (0, 0, 255)}

    for img, ann in tqdm(loader):
        image_id = ann['image_id'].numpy()[0][0]
        file_name = loader.dataset.coco.imgs[image_id]['file_name']
        img = torch_to_numpy(img)
        boxes = ann['boxes'][0].numpy()
        labels = ann['labels'][0].numpy()

        img_annotated = draw_bboxes(img, boxes, labels, colors)

        annotated_file_name = file_name.split('.')[0] + '_bbox.png'
        cv2.imwrite(os.path.join(argp.out_dir, annotated_file_name), img_annotated)
