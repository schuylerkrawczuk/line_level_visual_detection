import numpy as np


def relative_entropy(scores):
    """
        Calculate mean entropy of all image labels
    """
    return np.mean(-scores * np.log2(scores)).tolist()

