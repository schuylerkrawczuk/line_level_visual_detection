"""
    convert CVAT annotations to COCO json for compatibility with model pipeline
    yaml parameters:
        label_names (dict): CVAT label names for the annotated objects, i.e. table, column, cell, comment.
                            This option is here to normalize labels across different annotation sets w/ differing
                            label names. Must include 'comment' so that these can be removed
        input_path: path to xml file
        output_path: desired path of coco json output file
        train_set_ratio: percent of the images that will be separated into training set (vs. test set)
                         leave at 0 to keep all in one set
"""

from data_preparation.utils import train_split
import argparse
import xmltodict
import json
import yaml


def xml_bbox_to_json(box, category_id, img):
    """
    convert a bbox to json
    """
    ann = dict()
    ann['category_id'] = category_id
    ann['image_id'] = int(img['@id'])
    ann['bbox'] = [float(box['@xtl']), float(box['@ytl']),
                   float(box['@xbr']) - float(box['@xtl']),
                   float(box['@ybr']) - float(box['@ytl'])]
    ann['segmentation'] = [
        [float(box['@xtl']), float(box['@ytl']),
         float(box['@xbr']), float(box['@ytl']),
         float(box['@xbr']), float(box['@ybr']),
         float(box['@xtl']), float(box['@ybr'])]
    ]
    ann['area'] = ann['bbox'][2] * ann['bbox'][3]
    return ann


def ignore_box(box):
    """
    check if box is in default CVAT position
    """
    return (box['@xtl'] == '15.00'
            and box['@ytl'] == '15.00'
            and box['@xbr'] == '200.00'
            and box['@ybr'] == '100.00')


def xml_to_json(ann_path, new_ann_path, label_names):
    """
    convert xml to json annotation for compatibility w/ model
    """
    doc = xmltodict.parse(open(ann_path).read())['annotations']
    target_names = [i for i in label_names if i != 'comment']

    images = []
    annotations = []

    for ii, img in enumerate(doc['image']):
        if not isinstance(img['box'], list):
            img['box'] = [img['box']]

        image_json = dict()
        image_json['id'] = int(img['@id'])
        image_json['file_name'] = img['@name']
        image_json['height'] = int(img['@height'])
        image_json['width'] = int(img['@width'])
        images.append(image_json)

        for box in img['box']:
            # ignore images w/ comments
            if box['@label'] == label_names['comment']:
                if '#text' in box['attribute']:
                    continue

            if not ignore_box(box):
                for target_id, target in enumerate(target_names):
                    if box['@label'] == label_names[target]:
                        ann = xml_bbox_to_json(box, target_id + 1, img)
                        annotations.append(ann)

    for i in range(len(annotations)):
        annotations[i]['id'] = i

    categories = [{'name': target, 'id': target_id + 1, 'supercategory': None}
                  for target_id, target in enumerate(target_names)]

    annotation_to_dump = {'images': images,
                          'annotations': annotations,
                          'categories': categories
                          }
    with open(new_ann_path, 'w') as fp:
        json.dump(annotation_to_dump, fp)


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='convert xml to coco json')
    argc.add_argument('params', type=str, help='convertsion params')
    argp = argc.parse_args()
    params = yaml.safe_load(open(argp.params, 'r').read())
    output_dir = '/'.join(params['output_path'].split('/')[:-1])

    xml_to_json(params['input_path'], params['output_path'], params['label_names'])

    if params['train_set_ratio']:
        train_split(params['output_path'], output_dir, params['train_set_ratio'])
