# TODO: script this
"""
dilation transform used in CascadeTabNet (https://arxiv.org/abs/2004.12629) on general set of table images for training
"""
import os
import cv2
import glob
import numpy as np


if __name__ == '__main__':
    DATA_PATH = '/Users/schuylerkrawczuk/Desktop/work/data/visual_model_data/general_dataset'
    PATH_TO_ORIGIAL_IMAGES = os.path.join(DATA_PATH, 'orig_image/')
    PATH_TO_DEST = os.path.join(DATA_PATH, 'augmented_image/')

    if not os.path.isdir(PATH_TO_DEST):
        os.mkdir(PATH_TO_DEST)

    img_files = glob.glob(PATH_TO_ORIGIAL_IMAGES + "*.*")
    total = len(img_files)

    # 2x2 Static kernel
    kernel = np.ones((2, 2), np.uint8)

    for count, i in enumerate(img_files):
        image_name = i.split("/")[-1]
        print("Progress : ", count, "/", total)
        img = cv2.imread(i, 0)
        _, mask = cv2.threshold(img, 220, 255, cv2.THRESH_BINARY_INV)
        dst = cv2.dilate(mask, kernel, iterations=1)
        dst = cv2.bitwise_not(dst)
        cv2.imwrite(PATH_TO_DEST + "/dilation_" + image_name, dst)

