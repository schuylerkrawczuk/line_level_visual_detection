"""
    Evaluate the average precision at given IoU thresholds for image test set
    yaml parameters:
        dataset_class (str):          dataset class name from core.dataset
        img_dir (str):                directory of images to evaluate
        annotation_path (str):        path for gt coco annotation for images. If blank, only visualization will be done
        model_dir (str):              directory containing model to evaluate
        model_class (str):            class name of model. This should be included in models.models.py
        eval_dir (str):               directory to save evaluation results in
        checkpoint_file (str):        .pth checkpoint for trained model
        score_threshold (float):      confidence threshold for accepting a detection
        iou_thresholds (list[float]): list of IoU threhsolds to calculate average precision
        visualization (bool):         if true, images with pred/gt bounding boxes drawn on saved in eval_dir
        save_model_output (bool):     save output of model in array (n_detection x 6) [bbox, confidence, label]
        resize_images (bool):         resize all images to (1333, 800)
        nms (float):                  apply non-maximum suppression to output with given IoU threshold. If blank, op not performed
        return_entropy (bool):        returns CSV with relative entropy averaged across all labels for each image
"""

from core.transforms import get_transforms, torch_to_numpy
from data_preparation.visualization import draw_bboxes
from data_preparation.image_selection import relative_entropy
from core import dataset
from tqdm import tqdm
from typing import List, Sequence, Tuple
import models
import os
import argparse
import yaml
import torch
import torchvision
import cv2
import numpy as np
import pandas as pd


def run_eval(params):
    dataset_class = eval('dataset.{}'.format(params['dataset_class']))

    # load data
    transforms = get_transforms(eval=True, resize=params['resize_images'])
    dataset_params = {
        'img_dir': params['img_dir'],
        'annotation_path': params['annotation_path'],
        'transforms': transforms
    }
    if dataset_class is dataset.ImageTextDataSet:
        dataset_params['ocr_dir'] = params['ocr_dir']

    eval_dataset = dataset_class(**dataset_params)
    loader = torch.utils.data.DataLoader(
        eval_dataset,
        shuffle=False,
        num_workers=0,
        batch_size=1,
        collate_fn=lambda batch: tuple(zip(*batch))
    )

    # load model
    checkpoint = torch.load(os.path.join(params['model_dir'], params['checkpoint_file']), map_location=params['device'])
    model_class = eval('models.{}'.format(params['model_class']))
    model = model_class(pretrained=False, pretrained_backbone=True)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.to(params['device'])
    model.eval()

    # evaluate images
    if not os.path.isdir(params['eval_dir']):
        os.mkdir(params['eval_dir'])

    gt, preds, scores = [], [], []
    entropies = {'file_name': [], 'entropy': []}
    for img, ann in tqdm(loader):
        if params['annotation_path'] is not None:
            image_id = ann[0]['image_id'].numpy()[0]
            file_name = loader.dataset.coco.imgs[image_id]['file_name']
        else:
            file_name = ann[0]['file_name']

        if isinstance(img[0], torch.Tensor):
            img = [img.to(params['device']) for img in img]
        elif isinstance(img[0], dict):
            img = [{k: v.to(params['device']) for k, v in im.items()} for im in img]

        pred = model(img)
        if 'nms' in params and params['nms'] is not None:
            keep_inds = torchvision.ops.batched_nms(pred[0]['boxes'], pred[0]['scores'], pred[0]['labels'], params['nms'])
            pred[0]['boxes'] = pred[0]['boxes'][keep_inds]
            pred[0]['scores'] = pred[0]['scores'][keep_inds]
            pred[0]['labels'] = pred[0]['labels'][keep_inds]

        if params['save_model_output']:
            norm_boxes = pred[0]['boxes'].clone()
            norm_boxes[:, ::2] /= img[0].shape[2]
            norm_boxes[:, 1::2] /= img[0].shape[1]

            output_tensor = torch.cat([norm_boxes, pred[0]['scores'][:, None], pred[0]['labels'][:, None]], axis=1)
            pd.DataFrame(output_tensor.detach().numpy()).to_csv(
                os.path.join(params['eval_dir'], file_name.split('.')[0] + '_model_out.csv')
            )

        if params['annotation_path'] is not None:
            gt_boxes = ann[0]['boxes'].numpy()
            gt_labels = ann[0]['labels'].numpy()
            gt.append((gt_boxes, gt_labels))

        pred_boxes = pred[0]['boxes'].detach().cpu().numpy()
        pred_labels = pred[0]['labels'].detach().cpu().numpy()
        pred_scores = pred[0]['scores'].detach().cpu().numpy()

        preds.append((pred_boxes, pred_labels, pred_scores))

        entropies['entropy'].append(relative_entropy(pred_scores))
        entropies['file_name'].append(file_name)

        # save image w/ bboxes
        if params['visualization']:

            pred_box_color = {1: (255, 0, 0), 2: (51, 51, 0), 3: (255, 153, 51)}
            if isinstance(img[0], torch.Tensor):
                img = torch_to_numpy(img[0].cpu())
            elif isinstance(img[0], dict):
                img = torch_to_numpy(img[0]['image'].cpu())

            pred_boxes = np.array([bbox for bbox, score in zip(pred_boxes, pred_scores) if score > params['score_threshold']])
            img = draw_bboxes(img, pred_boxes, pred_labels, pred_box_color, scores=pred_scores)

            cv2.imwrite(os.path.join(params['eval_dir'], file_name), img)

    if 'return_entropy' in params and params['return_entropy']:
        pd.DataFrame(entropies).to_csv(os.path.join(params['eval_dir'], 'image_entropies.csv'))

    if params['annotation_path'] is not None:
        precisions = average_precision(gt, preds, params['score_threshold'], params['iou_thresholds'])
        pd.DataFrame(precisions).to_csv(os.path.join(params['eval_dir'], 'avg_precisions.csv'))
        print(precisions)


def average_precision(gt: List[Tuple], preds: List[Tuple],
                      score_threshold: float, iou_thresholds: List[float]):
    """
    calculate average precision at specified IoU thresholds given predicted+gt bboxes and scores
    """
    # TODO: remove redundancies from having threshold loop on the outside
    gt_boxes, gt_labels = map(list, zip(*gt))
    pred_boxes, pred_labels, pred_scores = map(list, zip(*preds))

    label_thresholds = {}
    for label in [1, 2, 3]:  # TODO: only use labels in gt
        threshold_precisions = {}
        for threshold in iou_thresholds:
            precisions = []
            for pred_box, pred_label, pred_score, gt_box, gt_label in zip(pred_boxes, pred_labels, pred_scores, gt_boxes, gt_labels):
                gt_box = [box for box, lab in zip(gt_box, gt_label) if lab == label]
                pred_box = [box for box, lab in zip(pred_box, pred_label) if lab == label]
                pred_score = [box for box, lab in zip(pred_score, pred_label) if lab == label]

                for bbox_g in gt_box:
                    gt_ious = [0]
                    for conf_p, bbox_p in zip(pred_score, pred_box):
                        iou = get_iou_score(bbox_p, bbox_g)
                        if conf_p < score_threshold or iou < threshold:
                            iou = 0
                        gt_ious.append(iou)
                    precisions.append(np.max(gt_ious))

            threshold_precisions[threshold] = np.mean(precisions)
        label_thresholds[label] = threshold_precisions

    return label_thresholds


def get_iou_score(box_a: Sequence, box_b: Sequence):
    """
    get IoU of two bboxes in (x0, y0, x1, y1) format
    """

    x0 = max(box_a[0], box_b[0])
    y0 = max(box_a[1], box_b[1])
    x1 = min(box_a[2], box_b[2])
    y1 = min(box_a[3], box_b[3])

    x_dist = 1 if x1 == x0 else x1 - x0
    y_dist = 1 if y1 == y0 else y1 - y0

    intersection_area = max(0.0, x_dist) * max(0.0, y_dist)

    # compute the area of union
    area_a = (box_a[2] - box_a[0]) * (box_a[3] - box_a[1])
    area_b = (box_b[2] - box_b[0]) * (box_b[3] - box_b[1])

    den = area_a + area_b - intersection_area
    den = 1 if den == 0 else den
    iou = intersection_area / den
    return round(iou, 6)


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='evaluate model')
    argc.add_argument('params', type=str, help='eval params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_eval(params)
