"""
    Trains an object detector given the following parameters
    yaml parameters:
        dataset_class (str):             dataset class name from core.dataset
        img_dir (str):                   directory of images to train/test on
        model_dir (str):                 directory for model being trained. Saves checkpoints/logs here
        model_class (str):               class name of model. This should be included in models.models.py
        checkpoint_file (str):           (optional) pth checkpoint if resuming training/ transfer learning/ fine-tuning
        annotation_path_train (str):     path to coco json annotation for training images
        annotation_path_val (str):       (optional) path to coco json annotation for validation images
        n_epochs (int):                  number of total training epochs
        fine_tune (bool):                if true, convolutional stem updated in training. False,  it's frozen
        resize_images (bool):            resize all images to (1333, 800)
"""

from core.transforms import get_transforms
from core.trainer import Trainer
from core import dataset
from torch.optim.lr_scheduler import MultiStepLR
import models
import torch
import argparse
import yaml


def run_training(params):
    dataset_class = eval('dataset.{}'.format(params['dataset_class']))

    # load data
    transforms = get_transforms(resize=params['resize_images'])
    train_dataset_params = {
        'img_dir': params['img_dir'],
        'annotation_path': params['annotation_path_train'],
        'transforms': transforms
    }
    if dataset_class is dataset.ImageTextDataSet:
        train_dataset_params['ocr_dir'] = params['ocr_dir']

    train_dataset = dataset_class(**train_dataset_params)
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        shuffle=True,
        num_workers=0,
        batch_size=2,
        collate_fn=lambda batch: tuple(zip(*batch))
    )
    data_loaders = {'train': train_loader}

    if 'annotation_path_val' in params and params['annotation_path_val'] is not None:
        transforms = get_transforms(eval=True, resize=params['resize_images'])
        val_dataset_params = {
            'img_dir': params['img_dir'],
            'annotation_path': params['annotation_path_val'],
            'transforms': transforms
        }
        if dataset_class is dataset.ImageTextDataSet:
            val_dataset_params['ocr_dir'] = params['ocr_dir']

        val_dataset = dataset_class(**val_dataset_params)
        val_loader = torch.utils.data.DataLoader(
            val_dataset,
            shuffle=False,
            num_workers=0,
            batch_size=2,
            collate_fn=lambda batch: tuple(zip(*batch))
        )
        data_loaders['val'] = val_loader

    # load model pretrained on imagenet
    model_class = eval('models.{}'.format(params['model_class']))
    pretrained = True if params['checkpoint_file'] is None else False
    model = model_class(pretrained=pretrained)

    # unfreeze backbone params for fine tuning
    if params['fine_tune']:
        for p in model.parameters():
            p.requires_grad = True

    model.to(params['device'])

    # initialize training
    trainable_params = [p for p in model.parameters() if p.requires_grad]

    # TODO: optim settings in params
    optimizer = torch.optim.SGD(trainable_params, lr=0.0012 / 100, momentum=0.9, weight_decay=0.0001)
    scheduler = MultiStepLR(optimizer, milestones=[16, 19])

    training_pieces = {
        'model': model,
        'optimizer': optimizer,
        'lr_scheduler': scheduler,
        'data_loaders': data_loaders,
    }

    # train
    trainer = Trainer(training_pieces, params)
    trainer.train(params['n_epochs'])


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='train model')
    argc.add_argument('params', type=str, help='training params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_training(params)
