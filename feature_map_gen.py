from core.transforms import get_transforms, torch_to_numpy
from core import dataset
import models
from tqdm import tqdm
import os
import argparse
import yaml
import torch
import numpy as np
import pandas as pd
import cv2


def run_eval(params):
    dataset_class = eval('dataset.{}'.format(params['dataset_class']))

    # load data
    transforms = get_transforms(eval=True, resize=params['resize_images'])
    dataset_params = {
        'img_dir': params['img_dir'],
        'annotation_path': params['annotation_path'],
        'transforms': transforms
    }
    if dataset_class is dataset.ImageTextDataSet:
        dataset_params['ocr_dir'] = params['ocr_dir']

    eval_dataset = dataset_class(**dataset_params)
    loader = torch.utils.data.DataLoader(
        eval_dataset,
        shuffle=False,
        num_workers=0,
        batch_size=1,
        collate_fn=lambda batch: tuple(zip(*batch))
    )

    # load graph data
    nodes_df = pd.read_csv(params['node_path'])


    # load model
    checkpoint = torch.load(os.path.join(params['model_dir'], params['checkpoint_file']), map_location=params['device'])
    model_class = eval('models.{}'.format(params['model_class']))
    model = model_class(pretrained=False, pretrained_backbone=True)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.to(params['device'])
    model.eval()

    # evaluate images
    if not os.path.isdir(params['eval_dir']):
        os.mkdir(params['eval_dir'])

    for img, ann in tqdm(loader):

        if params['annotation_path'] is not None:
            image_id = ann[0]['image_id'].numpy()[0]
            file_name = loader.dataset.coco.imgs[image_id]['file_name']
            if len(file_name) != 42:
                #print(file_name, 'fn')
                continue
        else:
            file_name = ann[0]['file_name']

        # load nodes for filename --> df and bbox
        file_mask = nodes_df.file == file_name.split('.')[0]
        bounding_boxes = nodes_df[file_mask][['x0', 'y0', 'x1', 'y1']].values

        if not len(bounding_boxes):
            #print(file_name, 'bbox')
            continue

        # fp on model
        if isinstance(img[0], torch.Tensor):
            img = [img.to(params['device']) for img in img]
        elif isinstance(img[0], dict):
            img = [{k: v.to(params['device']) for k, v in im.items()} for im in img]

        backbone_out = model.backbone(img[0][None, :])
        visual_fm = backbone_out['3'].detach().cpu()
        visual_fm = torch_to_numpy(visual_fm)
        visual_fm = cv2.resize(visual_fm, (800, 1344))
        visual_fm = visual_fm[:1333, :800, :]


        # resize normed boxes to match fm
        bounding_boxes[:, ::2] = (800 * bounding_boxes[:, ::2])
        bounding_boxes[:, 1::2] = (1333 * bounding_boxes[:, 1::2])
        bounding_boxes = bounding_boxes.astype(int)

        # for each bbox: get avg. pooled feature map, add to nodes data
        visual_features = []
        for box in bounding_boxes:
            node_features = visual_fm[box[1]: box[3], box[0]: box[2]].reshape(-1, visual_fm.shape[-1]).mean(axis=0)
            visual_features.append(node_features)

        # join features to node_df
        visual_features = np.vstack(visual_features)
        vf_cols = [f'vf_{i}' for i in range(visual_features.shape[1])]
        page_df = pd.DataFrame(visual_features, columns=vf_cols)
        page_df.index = nodes_df[file_mask].index
        nodes_df.loc[page_df.index, vf_cols] = page_df[vf_cols]

    nodes_df.to_csv(params['node_path'])


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='evaluate model')
    argc.add_argument('params', type=str, help='eval params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    params['node_path'] = '/data/home/schuyler/data/graph/13.2/nodes2.csv'


    run_eval(params)
