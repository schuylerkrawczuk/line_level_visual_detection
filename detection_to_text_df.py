from core.transforms import get_transforms
from core.dataset import ImageOnlyLoader
from torchvision.models.detection.faster_rcnn import fasterrcnn_resnet50_fpn
from tqdm import tqdm
import torchvision
import pandas as pd
import numpy as np
import os
import argparse
import yaml
import torch


def get_tokens_within_bbox(text_df, bbox):
    """
    Return sub-set of dataframe that is contained within bounding box
    """
    bbox_location = (text_df.x0 >= bbox[0] - 1) & \
                    (text_df.x1 <= bbox[2] + 1) & \
                    (text_df.y0 >= bbox[1] - 1) & \
                    (text_df.y1 <= bbox[3] + 1)

    return text_df[bbox_location]


def run_eval(params):

    # load data
    transforms = get_transforms(eval=True, resize=True)
    dataset_params = {
        'img_dir': params['img_dir'],
        'annotation_path': None,
        'transforms': transforms
    }
    eval_dataset = ImageOnlyLoader(**dataset_params)
    loader = torch.utils.data.DataLoader(
        eval_dataset,
        shuffle=False,
        num_workers=0,
        batch_size=1,
        collate_fn=lambda batch: tuple(zip(*batch))
    )

    # load model
    checkpoint = torch.load(os.path.join(params['model_path']), map_location=params['device'])
    model = fasterrcnn_resnet50_fpn(pretrained=False, pretrained_backbone=True)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.to(params['device'])
    model.eval()

    csv_files = os.listdir(params['csv_dir'])

    for img, ann in tqdm(loader):

        file_name = ann[0]['file_name']  # .png
        csv_file_name = file_name.split('.')[0] + '_0.csv'
        if csv_file_name not in csv_files:
            continue

        csv_path = os.path.join(params['csv_dir'], csv_file_name)
        text_df = pd.read_csv(csv_path)
        x_size = np.median(text_df['x1'] / text_df['x1_norm'])
        y_size = np.median(text_df['x1'] / text_df['y1_norm'])

        if isinstance(img[0], torch.Tensor):
            img = [img.to(params['device']) for img in img]
        elif isinstance(img[0], dict):
            img = [{k: v.to(params['device']) for k, v in im.items()} for im in img]

        pred = model(img)

        if params['nms'] is not None:
            keep_inds = torchvision.ops.batched_nms(pred[0]['boxes'], pred[0]['scores'], pred[0]['labels'], params['nms'])
            pred[0]['boxes'] = pred[0]['boxes'][keep_inds]
            pred[0]['scores'] = pred[0]['scores'][keep_inds]
            pred[0]['labels'] = pred[0]['labels'][keep_inds]

        confidences = pred[0]['scores'].detach().cpu().numpy()
        norm_boxes = pred[0]['boxes'].detach().cpu().numpy()
        norm_boxes = norm_boxes[confidences > params['score_threshold']]
        confidences = confidences[confidences > params['score_threshold']]

        norm_boxes[:, ::2] /= img[0].shape[2] / x_size
        norm_boxes[:, 1::2] /= img[0].shape[1] / y_size
        norm_boxes = norm_boxes

        # get contained subdf for each detection, mark visual_table_id
        text_df['visual_table_id'] = -1
        text_df['visual_table_confidence'] = -1
        for i, box in enumerate(norm_boxes):
            table_df = get_tokens_within_bbox(text_df, box)
            text_df.loc[table_df.index, 'visual_table_id'] = i
            text_df.loc[table_df.index, 'visual_table_confidence'] = confidences[i]

        text_df.to_csv(os.path.join(params['csv_dir'], csv_file_name))


if __name__ == '__main__':
    argc = argparse.ArgumentParser(description='evaluate model')
    argc.add_argument('params', type=str, help='eval params')
    argp = argc.parse_args()

    params = yaml.safe_load(open(argp.params, 'r').read())
    params['device'] = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    run_eval(params)
